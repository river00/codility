#include <stdio.h>

#include "solution.cpp"

void test(int v)
{
    if(v)
    {
        printf("OK\n");
    }
    else
    {
        printf("FAIL\n");
    }
}

int main(void)
{
    test(solution(32) == 0);

    return 0;
}
